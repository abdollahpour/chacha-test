
/* ------------------------------------------------------------ extra libs -------------------------------------------- */

/**
 * jQuery Plugin to obtain touch gestures from iPhone, iPod Touch and iPad, should also work with Android mobile phones (not tested yet!)
 * Common usage: wipe images (left and right to show the previous or next image)
 * 
 * @author Andreas Waltl, netCU Internetagentur (http://www.netcu.de)
 * @version 1.1.1 (9th December 2010) - fix bug (older IE's had problems)
 * @version 1.1 (1st September 2010) - support wipe up and wipe down
 * @version 1.0 (15th July 2010)
 */
(function($) { 
   $.fn.touchwipe = function(settings) {
     var config = {
			min_move_x: 20,
			min_move_y: 20,
			wipeLeft: function() { },
			wipeRight: function() { },
			wipeUp: function() { },
			wipeDown: function() { },
			preventDefaultEvents: true
	};
     
     if (settings) $.extend(config, settings);
 
     this.each(function() {
    	 var startX;
    	 var startY;
		 var isMoving = false;

    	 function cancelTouch() {
    		 this.removeEventListener('touchmove', onTouchMove);
    		 startX = null;
    		 isMoving = false;
    	 }	
    	 
    	 function onTouchMove(e) {
    		 if(config.preventDefaultEvents) {
    			 e.preventDefault();
    		 }
    		 if(isMoving) {
	    		 var x = e.touches[0].pageX;
	    		 var y = e.touches[0].pageY;
	    		 var dx = startX - x;
	    		 var dy = startY - y;
	    		 if(Math.abs(dx) >= config.min_move_x) {
	    			cancelTouch();
	    			if(dx > 0) {
	    				config.wipeLeft();
	    			}
	    			else {
	    				config.wipeRight();
	    			}
	    		 }
	    		 else if(Math.abs(dy) >= config.min_move_y) {
		    			cancelTouch();
		    			if(dy > 0) {
		    				config.wipeDown();
		    			}
		    			else {
		    				config.wipeUp();
		    			}
		    		 }
    		 }
    	 }
    	 
    	 function onTouchStart(e)
    	 {
    		 if (e.touches.length == 1) {
    			 startX = e.touches[0].pageX;
    			 startY = e.touches[0].pageY;
    			 isMoving = true;
    			 this.addEventListener('touchmove', onTouchMove, false);
    		 }
    	 }    	 
    	 if ('ontouchstart' in document.documentElement) {
    		 this.addEventListener('touchstart', onTouchStart, false);
    	 }
     });
 
     return this;
   };
 
 })(jQuery);

/*--------------------------------Some usefull functions-----------------------------*/
var supportsTransitions  = (function() {
    var s = document.createElement('p').style, // 's' for style. better to create an element if body yet to exist
        v = ['ms','O','Moz','Webkit']; // 'v' for vendor

    if( s['transition'] == '' ) return true; // check first for prefeixed-free support
    while( v.length ) // now go over the list of vendor prefixes and check support until one is found
        if( v.pop() + 'Transition' in s )
            return true;
    return false;
})();

function getTransitionDuration ($o) {
	if($o.css('transition-duration')) {
		return Math.round(parseFloat(this.css('transition-duration')) * 1000);
	} else {
		// check the vendor transition duration properties
		if($o.css('-webkit-transtion-duration')) return Math.round(parseFloat($o.css('-webkit-transtion-duration')) * 1000);
		if($o.css('-moz-transtion-duration')) return Math.round(parseFloat($o.css('-moz-transtion-duration')) * 1000);
		if($o.css('-ms-transtion-duration')) return Math.round(parseFloat($o.css('-ms-transtion-duration')) * 1000);
		if($o.css('-o-transtion-duration')) return Math.round(parseFloat($o.css('-ms-transtion-duration')) * 1000);
	}
	return 0;
}




var SERVER_PATH = "http://hamed.wpic-demo.com";
var SERVER_SLIDE_PATH = SERVER_PATH + "/api/list?search=$QUERY&start=$START&count=$SIZE";
var SERVER_SHARE_PATH = SERVER_PATH + "/api/share";
var SERVER_PACK_PATH = SERVER_PATH + "/api/pack";
var SERVER_SEARCH_MOST_PATH = SERVER_PATH + "/api/search/most";
var SERVER_SEARCH_MATCH_PATH = SERVER_PATH + "/api/search/match";

// @deprected
var __SERVER_PATH__ = SERVER_PATH;
var __SERVER_SLIDE_PATH__ = SERVER_SLIDE_PATH;

var TRANSIATION_SUPPORT = (function() {
	var s = document.createElement('p').style, // 's' for style. better to create an element if body yet to exist
		v = ['ms','O','Moz','Webkit']; // 'v' for vendor

	if( s['transition'] == '' ) return true; // check first for prefeixed-free support
	while( v.length ) // now go over the list of vendor prefixes and check support until one is found
		if( v.pop() + 'Transition' in s )
			return true;
	return false;
})();

var LOCAL_STORAGE_SUPPORT = (typeof(Storage)!=="undefined");







function PhoneApi() {

}

PhoneApi.prototype.share = function(text) {
	window.location = 'share:' + encodeURIComponent(text);
}


/**
 * Site manager
 */
function Chacha() {
	if(LOCAL_STORAGE_SUPPORT && typeof(localStorage.chacha_data) != 'undefined') {
		this._data = $.parseJSON(localStorage.chacha_data);
	} else {
		this._data = {};
	}
	this._fragment = {};
}

/**
 * REQUIRE CLASSES: .error, .loading
 * Load partial document in specific view
 */
Chacha.prototype.loadPartial = function(partial, $obj) {
	var self = this;
	var path = partial + ".html";
	
	$obj.empty();
	$("<div class='loading'></div>").appendTo($obj);
	
	$obj.load('partials/' + path, function(response, status, xhr) {
		if (status == "error") {
			// add error button to load it again
			$obj.empty();
			$("<div class='error'></div>").appendTo($obj).click(function() {
				self.loadPartial($obj);
			});
		} else {
			var fn = self._fragment[partial];
			if(typeof(fn) != 'undefined') {
				fn.call(self, $obj, true);
			}
		}
	});
}

Chacha.prototype.showPartial = function(name, $obj, overly) {
	// the content will load automatically if it needed
	if ($obj.children().length == 0){
		this.loadPartial(name, $obj);
	} else {
		var fn = this._fragment[name];
		if(typeof(fn) != 'undefined') {
			fn.call(self, $obj, false);
		}
	}
	
	// Setting will handle by cache
	//var hash = (overly == true ? '!' : '') + name;
	//if(window.location.hash != ('#' + hash)) {
	//	window.location.hash = hash;
	//}
	
	return false; // Allways return false
}

/**
* Convert hash mode to json object. In this version parameter does not support
* ex:
* hash1(subhash1+subhash2(subsubhash1))+hash2
*
*{
*  hash1: {
*    subs: [
*      subhash1: {}
*      subhash2: {
*        subs: [
*          subsubhash1: {}
*        ]
*      }
*    ]
*  },
*  hash2: {}
*}
*
*/
Chacha.prototype.hashToJson = function(hash) {
	// remove hash
	if(hash.indexOf('#') == 0) {
		hash = hash.substring(1);
	}

	var self = this;
	var hashs = [];
	
	var lastIndex = 0;
	var no = 0;
	
	for(var i=0; i<hash.length; i++) {
		var c = hash.charAt(i);
		
		if(c == ')') {
			no--;
		} else if(c == '(') {
			no++;
		} else if(c == '+') {
			if(no == 0) {
				hashs.push(hash.substring(lastIndex, i));
				lastIndex = i + 1; 
			}
		}
		if(i == hash.length - 1) {
			hashs.push(hash.substring(lastIndex));
		}
	}
	
	var data = {};
	
	$.each(hashs, function(i, v) {
		var index = v.indexOf('(');
		if(index > 0) {
			var subhash = v.substring(index + 1, v.length - 1);
			var hash = v.substring(0, index);

			data[hash] = {subs: self.hashToJson(subhash)};
		} else {
			data[v] = {};
		}
	});
	
	return data;
}

Chacha.prototype.jsonToHash = function(json) {
	var s = "";
	
	for(hash in json) {
		if(s.length > 0) {
			s += '+';
		}
		s += hash;

		if(typeof(json[hash].subs) != 'undefined') {
		
			s += '(' + this.jsonToHash(json[hash].subs) + ')';
		}
	}
	
	return s;
}



/**
 * We manage all the view and controllers with hash checking
 * This is hash format
 * #partial1{subPartial1:param1:param2}+partial2:param1+partial3	
 */ 
Chacha.prototype.hash = function(hashs, container) {
	// default hash
	if(typeof(hashs) == 'undefined' || typeof(container) == 'undefined') {
		hashs = this.hashToJson(document.location.hash);
		container = document.body;
	}

	function doHash(hash) {

		console.log('Show: ' + hash);

		// deactive the old ones
		$("> [partial='active']", container).each(function() {
			var active = $(this);
			var partialName = active.attr('hash');
			
			if(typeof(hashs[partialName]) == 'undefined') {
				console.log("hide " + partialName + " " + hashs[partialName]);
				active.attr('partial', '');
		
				// TODO: Some problem to get transition time
				var sleep = TRANSIATION_SUPPORT ? 500 : 0;//getTransitionDuration(active);
				if(sleep > 0) {
					active.removeClass('active');
					setTimeout(function() {active.removeClass('append')}, sleep);
				} else {
					active.removeClass('active');
					active.removeClass('append');
				}
			}
		});
		
		
		var partialContainer = $("> div[hash='" + hash + "']", container);
		if(partialContainer.length > 0) {
			this.showPartial(hash, partialContainer);
			
			if(partialContainer.attr('partial') != 'active') {
				partialContainer.attr('partial', 'active');
			
				if(TRANSIATION_SUPPORT) {
					partialContainer.addClass('append');
					var p = partialContainer;
					setTimeout(function() {p.addClass('active')}, 10);
				} else {
					partialContainer.addClass('append');
					partialContainer.addClass('active');
				}
			}
			
			var sub = hashs[hash].subs;
			
			if(typeof(sub) != 'undefined') {
				
				this.hash(sub, partialContainer);
			}
		} else {
			console.error(hash + " partial not found!");
		}

	}

	for(hash in hashs) {
		doHash.call(this, hash);
	}
}

Chacha.prototype.data = function(data, value) {
	if(typeof(data) != 'undefined') {
		if(typeof(data) == 'string') {
			if(typeof(value) != 'undefined') {
				// remove
				this._data[data] = value;
				
				if(LOCAL_STORAGE_SUPPORT) {
					localStorage.chacha_data = JSON.stringify(this._data);
				}
			}
			return this._data[data];
		} else {
			$.extend(this._data, data);
			
			if(LOCAL_STORAGE_SUPPORT) {
				localStorage.chacha_data = JSON.stringify(this._data);
			}
		}
	} else {
		return this._data;
	}
}

Chacha.prototype.fragment = function(name, fn) {
	if(typeof(name) != 'undefined') {
		if(typeof(fn) != 'undefined') {
			this._fragment[name] = fn;
		}
		
		return this._fragment[name];
	}
	
	return this._fragment;
}

Chacha.prototype.show = function(name, overly, data) {
	if(typeof(data) != 'undefined') {
		this.data(data);
	}
	
	// convert name to hash1
	// ex:
	// hash1.subhash1.subsubhash2
	// {hash1: {subs: {subhash1: {subs: {subsubhash2}}}}}
	var parts = name.split('.');
	var p;
	
	for(var i=parts.length - 1; i >= 0; i--) {
		var n = {};
		n[parts[i]] = {};
				
		if(typeof(p) == 'undefined') {
			p = n;
		} else {
			n[parts[i]].subs = p;
			p = n;
		}
	}

	if(overly == true && window.location.hash) {
		var current = this.hashToJson(window.location.hash);
		$.extend(p, current);
	}
	window.location.hash = this.jsonToHash(p);
}

/** ----------------------- slidetouch data source with paging abilities ----------------------------- **/




$(document).ready(function() {
	// Initilize page
	window.chacha = new Chacha();
	window.onhashchange = function() {
		chacha.hash();
	};

	window.chacha.hash();
	
	// Android 4.x could not load file with hash so, we load it and then set the hash
	if(!window.location.hash) {
		location.replace("#help");
	}

	// TEST
	//var hash = window.chacha.jsonToHash({hash1: {subs: {subhash1: {}, subhash2: {}}}, hash2: {}});
	//hash = window.chacha.hashToJson(hash);
	//hash = window.chacha.jsonToHash(hash);
	//alert(hash);
	
	window.phoneApi = new PhoneApi();
});
