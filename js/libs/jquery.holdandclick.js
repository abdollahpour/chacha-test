/**
 * Press and hold handler event for JQuery
 * 
 * @author Hamed Abdollahpour (http://web-presence-in-china.com/holdandclick)
 * @version 0.1 (3th Septemberxs 2013)
 */
(function($) {

$.fn.holdandclick = function(hold, click, timeout) {
	if(typeof(click) == 'Number') {
		timeout = click;
		click = undefined;
	}
	if(typeof(timeout) == 'undefined') {
		timeout = 600;
	}
	
	if(typeof(click) != 'undefined') {
		$(this).click(click);
	}
	
	if(typeof(hold) != 'undefined') {
		this.each(function(i, v) {
			var fired = false;
			var timeoutId;
			
			function fire() {
				timeoutId = setTimeout(function() {
					if(typeof(click) == 'function') {
						$(v).unbind('click');
					}
					
					fired = true;
					hold.call(v);
				}, timeout);
			}
			
			function backToNormal() {
				if(fired) {
					fired = false;
					if(typeof(click) == 'function') {
						setTimeout(function() {$(v).click(click)}, 10);
					}
				}
				clearTimeout(timeoutId);
			}
			
			if('ontouchstart' in document.documentElement) {
				$(this).bind('touchstart', fire).bind('touchend', backToNormal);
			} else {
				$(this).mousedown(fire).bind('mouseup mouseleave mouseenter', backToNormal);
			}
		});
	}
	return this;
}
 
})(jQuery);
